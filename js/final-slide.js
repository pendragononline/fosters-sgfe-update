var $ = function (id) { return document.getElementById(id); };

function revealQuote() {
    setTimeout(function () {
        $('blockquote-section').classList.remove("hidden");
        $('blockquote-section').classList.add("fade-in-animation");
    }, 2000);
    setTimeout(function () {
        $('blockquote-section').classList.remove("fade-in-animation");
        $('blockquote-section').classList.add("fade-out-animation");
        $('slide5').classList.remove("slide-bg-original");
        $('slide5').classList.add("slide-gradient");
    }, 17000);
    setTimeout(function () {
        $('blockquote-section').classList.remove("fade-out-animation");
        $('blockquote-section').classList.add("hidden");
        displayCredits();
    }, 21000);
}

function displayCredits() {
    $('credits-section').classList.remove("hidden");
    $('pendragon-logo').classList.remove("invisible");
    $('pendragon-logo').classList.add("zoom-in-left-animation");
    $('fosters-logo').classList.remove("invisible");
    $('fosters-logo').classList.add("zoom-in-right-animation");
    setTimeout(function () {
        $('produced-msg').classList.remove("invisible");
        $('produced-msg').classList.add("fade-in-up");
    }, 2000);
}