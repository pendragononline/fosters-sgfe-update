var $ = function (id) { return document.getElementById(id); };
var panelOpen = 0;

function info() {
    if (panelOpen === 0) {
        $('info-button').classList.add("slide-button-active");
        $('info-panel').classList.remove("hide-map-panel");
        $('info-panel').classList.add("show-map-panel");
        setInfo();
        panelOpen = 1;
    }
    else {
        closeInfoPanel(); 
    }
}

var slideDescriptions = [
    "This presentation introduces you to the culture at Fosters Solicitors LLP and how we came up with our main values.",
    "The values we felt best represent the Fosters culture are: Simply Transparent, Unity & Collaboration and Building Relationships.",
    "Staff were asked to consider a phrase that accurately describes the values and create a short summary of each of these.",
    "The positive and negative behaviours of each of the three phrases were considered in a workshop that staff from all branches were invited to attend.",
    "Our aim is to have a strong culture that is attractive to new members of staff so that we attract high quality, like-minded people, who will help develop the firm further and also to support the staff that we already have so that retention is good and staff feel valued, supported, included and at the heart of what we do.",
    "Culture isn't magic, it's having a shared goal and ensuring that that goal is reached."
];

function setInfo() {
    $('slide-info').innerHTML = slideDescriptions[slide];
}

function closeInfoPanel() {
    $('info-panel').classList.add("hide-map-panel");
    $('info-panel').classList.remove("show-map-panel");
    $('slide-info').innerHTML = "";
    $('info-button').classList.remove("slide-button-active");
    panelOpen = 0;
}