var $ = function (id) { return document.getElementById(id); };
var phraseIndex = 0;
var phrasesArray = [
    ["What do these values mean?", "Simply Transparent", "Unity & Collaboration", "Building Relationships", "Next steps"],
    ["We then worked with our staff to identify the meaning of each of these values.",
        "Being clear, fair and accountable for our decisions and actions.",
        "Being part of an organisation where together we are all moving in the same direction.",
        "Working with clients and colleagues and respecting their beliefs and values to create a happy working environment.",
        "We then identified the positive and negative behaviours of each of the three values. Click the 'next' button to find out more."],
    ["images/community.svg", "images/transparent.svg", "images/unity.svg", "images/relationships.svg", "images/career.svg"]
];

//REMOTE FUNCTION
var remotePhrases = function (e) {
    if (event.keyCode === 34) {
        switchPhrase();
    }
};

function phrases() {
    if (slide !== 2) {
        console.log("PHRASES SCRIPT TERMINATED");
        return;
    }
    $('theme-name-text').innerHTML = phrasesArray[0][phraseIndex];
    $('theme-description-text').innerHTML = phrasesArray[1][phraseIndex];
    $('phrases-slide-bg').style.backgroundImage = "url(" + phrasesArray[2][phraseIndex] + ")";
    setTimeout(function () {
        $('phrase-container').classList.remove("hidden");
        $('phrase-container').classList.add("zoom-in-left-animation-fast");
        $('phrases-slide-bg').classList.remove("hidden");
        $('phrases-slide-bg').classList.add("zoom-in-animation-fast");
    }, 2000);
    document.addEventListener("keyup", remotePhrases); //ADD REMOTE FUNCTION
    document.getElementById('next').removeEventListener("click", nextSlide); //REMOVE SLIDE ADVANCE
    document.getElementById('next').addEventListener("click", switchPhrase); //'NEXT' BUTTON NOW ACTIVATES SWITCH PHRASE FUNCTION
}

function switchPhrase() {
    setTimeout(function () {
        $('phrase-container').classList.remove("zoom-in-left-animation-fast");
        $('phrase-container').classList.add("zoom-out-up-animation-fast");
        $('phrases-slide-bg').classList.remove("zoom-in-animation-fast");
        $('phrases-slide-bg').classList.add("zoom-out-down-animation-fast");
    }, 1000);
    setTimeout(function () {
        clearPhrases();
    }, 3000);
    setTimeout(function () {
        phraseIndex++;
        if (phraseIndex > 4) {
            document.removeEventListener("keyup", remotePhrases); //REMOVE REMOTE FUNCTION
            document.getElementById('next').removeEventListener("click", switchPhrase); //REMOVE PHRASE ADVANCE
            document.getElementById('next').addEventListener("click", nextSlide); //'NEXT' BUTTON ADVANCES FRAME AGAIN
            nextSlide(); //slide.js
        }
        $('phrase-container').classList.remove("zoom-out-up-animation-fast");
        $('phrase-container').classList.add("zoom-in-left-animation-fast");
        $('phrases-slide-bg').classList.remove("zoom-out-down-animation-fast");
        $('phrases-slide-bg').classList.add("zoom-in-animation-fast");
    }, 3000);
    setTimeout(function () {
        phrases();
    }, 3500);
}

function clearPhrases() {
    $('theme-name-text').innerHTML = "";
    $('theme-description-text').innerHTML = "";
    $('phrases-slide-bg').style.backgroundImage = "";
}


