var attentionseekers = ['jello-animation', 'wobble-animation', 'tada-animation', 'swing-animation'];
var animationfadeins = ['fade-in', 'fade-in-down', 'fade-in-left', 'fade-in-right', 'fade-in-up', 'fade-in-up-std'];
var animationentrances = ['bounce-in-right-animation', 'bounce-in-left-animation', 'bounce-in-up-animation', 'bounce-in-down-animation', 'lightspeed-in-animation', 'zoom-in-down-animation', 'zoom-in-animation', 'zoom-in-up-animation', 'zoom-in-left-animation', 'zoom-in-right-animation', 'rotate-in-animation'];
var animationexits = ['rotate-out-animation', 'zoom-out-down-animation', 'zoom-out-up-animation', 'bounce-out-animation', 'lightspeed-out-animation'];

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}