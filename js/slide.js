var slide = 0;
var noOfSlides = document.getElementsByClassName("slide").length;
var pageTitle = "";
var titleExt = " | Fosters Culture";
var titleCntr = 0;
var menuButtons = document.getElementsByClassName("menu-item");

//Automatically set body width
var bodyWidth = (noOfSlides * 100).toString();
document.body.style.width = bodyWidth + "vw";

//Titles
var pageTitles = [
    'Introduction',
    'Our Values',
    'Our Values',
    'Behaviours',
    'Behaviours',
    'Thank You!'
];

//Navigate
document.getElementById('next').addEventListener("click", nextSlide); 

//Keyboard buttons
document.addEventListener("keyup", function (event) {
    if (event.keyCode === 32 || event.keyCode === 39 || event.KeyCode === 40) {
        event.preventDefault();
        nextSlide();
    }
    //REMOTE
    if (event.keyCode === 34) {
            event.preventDefault();
            nextSlide();
    }
});

document.addEventListener("keyup", function (event) {
    if (event.keyCode === 122) {
        presentationMode();
    }
});

//Presentation mode
function presentationMode() {
    document.body.classList.add("no-cursor");
    $('info-button').classList.add("invisible");
    $('next').classList.add("invisible"); 
    $('traffic-light-caption').innerHTML = "The behaviours associated with each value.";
}

function nextSlide() {
    if (panelOpen == 1) {
        closeInfoPanel();
    }
    titleCntr++;
    slide++;
    checkPosition();
    navigate();
    documentTitle(titleCntr);
    checkFunctionality(slide);
}

function checkPosition() {
    if (titleCntr > noOfSlides - 2) {
        titleCntr = 0;
    }
    if (titleCntr < 0) {
        titleCntr = noOfSlides - 2;
    }
}

function navigate() {
    var newSlide = document.getElementById('slide' + slide);
    var slideToNext = new ScrollTo({
        target: newSlide,
        axis: 'x',
        animationFn: 'easeOut',
        duration: 1200
    });
    slideToNext.scroll();
}

function documentTitle(titleCntr) {
    document.title = pageTitles[titleCntr] + titleExt;
}

function removeActiveStatus() {
    for (i = 0; i < menuButtons.length; i++) {
        menuButtons[i].classList.remove("active-phase");
    }
}

//Make next button bounce afer four seconds
var bouncingButton;
function nextBounce() {
    bouncingButton = setTimeout(buttonBounce, 8000);
}
function buttonBounce() {
    document.getElementById('next').classList.add("shake-animation");
}
function stopBounce() {
    clearTimeout(bouncingButton);
}
nextBounce();

function checkFunctionality(slide) {
    if (slide === 1) {
        stopBounce();
        document.getElementById("next").classList.remove("shake-animation");
        document.getElementById("fosters-values-triangle").classList.remove("hidden");
    }

    if (slide === 2) {
        phrases(); //phrases.js
    }

    if (slide === 3) {
        cityAnimation(); //values.js
    }

    if (slide === 4) {
        setTimeout(function () {
            document.getElementById("whose-values-container").classList.remove("hidden");
        }, 500);
    }

    if (slide === 5) {
        revealQuote(); //final-slide.js
        $('next').classList.add("fade-out-animation");
    } 
}






