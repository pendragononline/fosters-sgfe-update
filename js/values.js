var $ = function (id) { return document.getElementById(id); };
var valueTitle = "";
var currentThemeIndex = -1;
var trafficLightClick = -1;
var arrayToUse = "";
var trafficLightColour = "red"; //default
var cityZoom = 0;
var lightFunctions = ["redLight", "greenLight"];
var titleValue = ["To be SIMPLY TRANSPARENT,", "To have UNITY & COLLABORATION,", "To BUILD RELATIONSHIPS,"];

var doValues = [
    /*SIMPLY TRANSPARENT DO*/["Keep our costs transparent", "Manage expectations and ensure we are honest about our capabilities", "Admit our mistakes and accept responsibility", "Invest time to make sure people are given the information they need", "Give people regular and timely feedback"],
    /*UNITY & COLLABORATION DO*/["Look for every opportunity to cross-refer business", "Promote inclusivity - there is no hierarchy", "Positively work towards shared goals, knowing the objectives", "Support decisions that are made, even if not in agreement with them", "Listen to other people's opinions - everyone has a voice", "Go the extra mile", "Support and encourage professional development"],
    /*BUILDING RELATIONSHIPS DO*/["Celebrate colleagues' success", "Recognise and reward outstanding performance", "Embrace diversity", "Take time to understand other people's situations", "Provide consistency of representation", "Consistently aim to include everyone and provide positive communication", "Develop positive relationships with the wider community"]   
];

var dontValues = [
    /*SIMPLY TRANSPARENT DON'T*/["Create unnecessary work with a view to charge more", "Over-promise and under-deliver", "Refuse to accept responsibility or pass the buck", "Give vague information, use legal jargon or hide information and knowledge"],
    /*UNITY & COLLBORATION DON'T*/["Refuse to share information across departments", "Act politically", "Just think of ourselves", "Speak critically and publicly about decisions that have been made", "Ignore other people's opinions", "Do the bare minimum", "Let our colleagues sink"],
    /*BUILDING RELATIONSHIPS DON'T*/["Get jealous when others achieve", "Encourage unhealthy competition", "Discriminate against others", "Assume we know or have the full information", "Dismiss goals or aim to be obstructive", "Gossip about colleagues", "Think short term"]
];

var grads = ["sunset-grad", "pink-blue-grad", "yellow-pink-grad", "red-orange-grad"];

//REMOTE FUNCTION
var remoteTrafficLights = function (e) {
    if (event.keyCode === 34) {
        advanceTrafficLights();
    }
};


//ADVANCE TRAFFIC LIGHTS
function advanceTrafficLights() {
    trafficLightClick++;
    if (trafficLightClick > 1) {
        trafficLightClick = 0;
    }
    window[lightFunctions[trafficLightClick]]();
}

function cityAnimation() { //ENTRY POINT
    //EVENT LISTENERS
    document.getElementById('next').removeEventListener("click", nextSlide); //REMOVE SLIDE ADVANCE FROM NEXT BUTTON
    document.getElementById('next').addEventListener("click", advanceTrafficLights); //'NEXT' BUTTON ADVANCES TRAFFIC LIGHTS
    document.addEventListener("keyup", remoteTrafficLights); //ADD REMOTE FUNCTION
    setTimeout(function () {
        $('city').classList.add("city-move-0");
        enterCar();
    }, 2000);
    setTimeout(function () {
        $('value-words').classList.remove("hidden");
        $('value-words').classList.add("zoom-in-left-animation");
    }, 6000);
    setTimeout(function () {
        $('traffic-light').classList.remove("hidden");
        $('traffic-light').classList.add("zoom-in-up-animation");
    }, 7000);
    setTimeout(function () {
        $('red-light').classList.add("red-active");
    }, 8000);
}

function enterCar() {
    $('car-container').classList.remove("hidden");
}

function hideAndSwap() {
    setTimeout(function () {
        $('value-words').classList.remove("zoom-in-left-animation");
        $('value-words').classList.add("zoom-out-up-animation");
    }, 1000);
    setTimeout(function () {
        resetWords();
    }, 3000);
    setTimeout(function () {
        $('value-words').classList.remove("zoom-out-up-animation");
        $('value-words').classList.add("zoom-in-left-animation");
    }, 4500);  
}

function resetWords() {
    $('value-title').innerHTML = "";
    $('traffic-light-caption').innerHTML = "";
    $('values-list').remove();
}

function redLight() {
    hideAndSwap();
    $('green-light').classList.remove("green-active");
    $('red-light').classList.add("red-active");
    trafficLightColour = "red";
    currentThemeIndex++;
    setWords(currentThemeIndex);
}

function greenLight() {
    hideAndSwap();
    cityMove();
    trafficLightColour = "green";
    setWords(currentThemeIndex);
    $('red-light').classList.remove("red-active");
    $('green-light').classList.add("green-active"); 
}

function setWords(currentThemeIndex) {
    if (currentThemeIndex > 2) {
        $('car-container').classList.add("hidden");
        document.removeEventListener("keyup", remoteTrafficLights); //REMOVE REMOTE FUNCTION
        document.getElementById('next').removeEventListener("click", advanceTrafficLights); //REMOVE TRAFFIC LIGHT ADVANCE
        document.getElementById('next').addEventListener("click", nextSlide); //REINSTATE SLIDE ADVANCE
        nextSlide(); //slide.js
    }
    else {
        setTimeout(function () {
            $('value-title').innerHTML = titleValue[currentThemeIndex];
            createValuesList(currentThemeIndex);
        }, 5000);  
     }   
}

function createValuesList(currentThemeIndex) {
    var valueSection = $('value-words');
    var ul = document.createElement('ul');
    ul.setAttribute("id", "values-list");
    if (trafficLightColour === "red") {
        populateListDont(ul, valueSection, currentThemeIndex);
    }
    if (trafficLightColour === "green") {
        populateListDo(ul, valueSection, currentThemeIndex);
    }
}

function populateListDont(ul, valueSection, currentThemeIndex) {
    for (var i = 0; i < dontValues[currentThemeIndex].length; i++) {
        var li = document.createElement('li');
        li.appendChild(document.createTextNode(dontValues[currentThemeIndex][i]));
        ul.appendChild(li);
    }
    valueSection.appendChild(ul);
    $('traffic-light-caption').innerHTML = "WE DON'T...";
}

function populateListDo(ul, valueSection, currentThemeIndex) {
    for (var i = 0; i < doValues[currentThemeIndex].length; i++) {
        var li = document.createElement('li');
        li.appendChild(document.createTextNode(doValues[currentThemeIndex][i]));
        ul.appendChild(li);
    }
    valueSection.appendChild(ul);
    $('traffic-light-caption').innerHTML = "WE DO...";
}

function cityMove() {
    cityZoom++;
    if (cityZoom > 3) {
        return;
    }
    var cityZoomPrev = cityZoom - 1;
    setTimeout(function () {
        $('car').classList.add("city-car-drive-animation-" + cityZoom);
        $('city').classList.remove("city-move-"+cityZoomPrev);
        $('city').classList.add("city-move-" + cityZoom);
        if (cityZoom === 1) {
            $('slide3').classList.remove("blue-sky-grad");
        }
        $('slide3').classList.remove("bg-"+cityZoomPrev);
        $('slide3').classList.add(grads[cityZoom]);
    }, 1000);
}
